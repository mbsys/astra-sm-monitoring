#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;

use Unix::PID;
use Getopt::Std;

our %opts;

getopts("p:", \%opts);

my $pid = 0;

if ($opts{p}) {
    $pid = $opts{p}
}

if ($pid) {
    my $procChecker = Unix::PID->new();
    if ($procChecker->is_pid_running($pid)) {
        print 1;
    }
    else{
        print 0;
    }
}
else{
    exit 0;
}

