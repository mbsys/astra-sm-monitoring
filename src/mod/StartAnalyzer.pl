#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;

use Getopt::Std;
use Proc::Background;

our %opts;

getopts("c:", \%opts);

my $cli = '';

if ($opts{c}) {
    $cli = $opts{c}
}
else{
    exit 1;
}

$cli = $cli . '> /dev/null 2>&1' . '&';

my $procLauncher = Proc::Background->new($cli);

exit 0;