#!/usr/bin/perl -w
use strict;
use warnings;
use diagnostics;

use Getopt::Std;
use Proc::Background;
use File::Which qw(which);
use FindBin '$Bin';

my $config;
$config->{SDVmp2Amp2} = 'udp://232.90.0.1:1234';
$config->{SDVmp4Amp2} = 'udp://232.90.0.2:1234';
$config->{HDVmp4Amp2} = 'udp://232.90.0.3:1234';

my %opts;
getopts('n:a:l:t:f:p:s:d:q:bw:', \%opts);
if ($opts{w}) {
    $config->{wdir} = $opts{w};
    if ($config->{wdir} =~ /^\//) {}
    else{
        $config->{wdir} .= $Bin. '/' . $config->{wdir};
    }
}
else{ $config->{wdir} = $Bin }

if (!(-d $config->{wdir})) {
    my $c = 'mkdir -p '.$config->{wdir};
    my $exitCode = system($c);
    if ($exitCode != 0) {
        warn ("Can\'t make directory at $config->{wdir} : $!");
        warn ("Exit code: %d. Terminating.", $? >> 8);
        exit -1;
    }
}

if ($opts{n}) { $config->{instance_name} = $opts{n} }
else{ $config->{instance_name} = 'astra' };

if ($opts{q}) { $config->{filename} = $config->{wdir}.'/'.$opts{q} }
else{ $config->{filename} = $config->{wdir}.$config->{instance_name}.'.txt' };

if ($opts{b}) { $config->{transponder_type} = 1 }
else{ $config->{transponder_type} = 0 }

if ($opts{a}) { $config->{adapter} = $opts{a} }
else{ $config->{adapter} = 0 }

if ($opts{l}) { $config->{lnb} = $opts{l} }
else{ $config->{lnb} = '9750:10600:11700' }

if ($opts{t}) { $config->{dvbs_type} = $opts{t} }
else{ $config->{dvbs_type} = 'S' }

if ($opts{f}) { $config->{frequency} = $opts{f} }
else{
    if (!$opts{q}) {
        warn("Frequency not defined. Exiting.");
        exit -1;
    }
}

if ($opts{p}) { $config->{pol} = $opts{p} }
else{
    if (!$opts{q}) {
        warn("Polarisation not defined. Exiting.");
        exit -1;
    }
}

if ($opts{s}) { $config->{symbol_rate} = $opts{s} }
else{
    if (!$opts{q}) {
        warn("Symbol rate not defined. Exiting.");
        exit -1;
    }
}

if ($opts{d}) { $config->{diseqc} = $opts{d} }
else{ $config->{diseqc} = 0 }

if (!$opts{q}) {
    # Generate cli command
    $config->{Path}->{astra} = which 'astra';
    $config->{cli} = $config->{Path}->{astra};
    $config->{cli} .= ' --analyze -n 3';
    
    # astra --analyze -n 3 "dvb://#adapter=0&lnb=10750:10750:10750&type=S2&tp=11996:H:27500&diseqc=2" > /root/astra.txt
    # astra --analyze -n 3 "dvb://#adapter=0&lnb=10750:10750:10750&type=S2&tp=11996:H:27500&diseqc=2"> 36_11996_H.txt
    # astra --analyze -n 3 "dvb://#adapter=0&lnb=10750:10750:10750&type=S2&tp=11996:H:27500&diseqc=2"> /home/mehdi/Projects/Perl/astra-parser/36_11996_H.txt
    # -n 36_11996_H -a 0 -f 11996 -p H -s 27500 -l 10750:10750:10750 -t S2 -d 2 -b 
    
    $config->{dvb_uri} = 'dvb://#';
    $config->{dvb_uri} .= 'adapter=' . $config->{adapter};
    $config->{dvb_uri} .= '&lnb=' . $config->{lnb};
    $config->{dvb_uri} .= '&type=' . $config->{dvbs_type};
    $config->{dvb_uri} .= '&tp=' . $config->{frequency} . ':' . $config->{pol} . ':' . $config->{symbol_rate};
    if ($config->{diseqc}) {
        $config->{dvb_uri} .= '&diseqc=' . $config->{diseqc};
    }
    $config->{cli} .= ' "' . $config->{dvb_uri} . '"';
    $config->{cli} .= ' > ' . $config->{filename};
    
    my $procLauncher = Proc::Background->new($config->{cli});
    $procLauncher->wait();
}

my ($fileContent, $nextline, $jsonObject, $tmpArray, $nLine);

if(!(-e $config->{filename})) {
    warn "Input file does not exists. Exiting";
    exit -1;
}

open(ATXT, '<', $config->{filename}) or die "Cant open file: $!";

FIRSTSTEPREADER:
while ($nextline = <ATXT>) {
    $nextline =~ s/[a-zA-z]{3}\s{1}\d{2}\s{1}\d{2}:\d{2}:\d{2}:\s{1}//;
    $nextline =~ s/\n+//g;
    $nextline =~ s/\r+//g;
    
    if ($nextline =~ /starting astra/i) { next FIRSTSTEPREADER; }
    elsif($nextline =~ /error/i){ next FIRSTSTEPREADER; }
    
    $nextline =~ s/info:\s{1}//i;
    push (@{ $fileContent }, $nextline);
    undef $nextline;
}
close ATXT;

if (!$opts{q}) { unlink $config->{filename}; }

ARRAYPARSER:
for(my $i = 0; $i < scalar(@{ $fileContent }); $i++){
    $nextline = $fileContent->[$i];
    if ($nextline =~ /^pat/i) {
        if ($nextline =~ /tsid/i) {
            $nextline =~ s/\D//g;
            $jsonObject->{TSID} = $nextline;
            undef $nextline;
            next ARRAYPARSER;
        }
        elsif($nextline =~ /nit/i){
            $nextline =~ s/\D//g;
            $jsonObject->{NIT} = $nextline;
            undef $nextline;
            next ARRAYPARSER;
        }
        elsif($nextline =~ /pid/i){
            $nextline =~ s/pat:\s{1}//i;
            @{ $tmpArray } = split(/\s+/, $nextline);
            push( @{ $jsonObject->{PNR_ARRAY} }, $tmpArray->[4]);
            $jsonObject->{PNRS}->{$tmpArray->[4]}->{PMT} = $tmpArray->[1];
            undef $tmpArray;
            undef $nextline;
            next ARRAYPARSER;
        }
        elsif($nextline =~ /crc32/i){
            undef $nextline;
            next ARRAYPARSER;
        }
    }
    elsif($nextline =~ /^cat/i){
        undef $nextline;
        next ARRAYPARSER;
    }
    elsif($nextline =~ /^pmt/i){
        my $nPNR = 0;
        PMTPARSER:
        while(1){
            $nLine = $fileContent->[$i];
            #print $nLine, "\n";
            if ($nLine =~ /pnr/i) {
                $nLine =~ s/\D//g;
                $nPNR = $nLine;
            }
            elsif($nLine =~ /pcr/i){
                $nLine =~ s/\D//g;
                $jsonObject->{PNRS}->{$nPNR}->{PCR} = $nLine;
            }
            elsif($nLine =~ /^video/i){
                if (($nLine =~ /pid/i) and ($nLine =~ /type/i)) {
                    @{ $tmpArray } = split(/\s+/, $nLine);
                    
                    for(my $z = 0; $z < scalar( @{ $tmpArray } ); $z++){
                        if ($tmpArray->[$z] =~ /pid/i) {
                            $jsonObject->{PNRS}->{$nPNR}->{VIDEO}->{PID} = $tmpArray->[$z + 1];
                        }
                        elsif($tmpArray->[$z] =~ /type/i){
                            $jsonObject->{PNRS}->{$nPNR}->{VIDEO}->{TYPE} = $tmpArray->[$z + 1];
                            $jsonObject->{PNRS}->{$nPNR}->{VIDEO}->{CODEC} = $tmpArray->[$z + 2];
                        }
                    }
                    undef $tmpArray;
                }
                elsif($nLine =~ /stream id/i){
                    $nLine =~ s/\D//g;
                    $nLine =~ s/\s+//g;
                    $jsonObject->{PNRS}->{$nPNR}->{VIDEO}->{StreamIndex} = $nLine;
                }
            }
            elsif($nLine =~ /^audio/i){
                if (($nLine =~ /pid/i) and ($nLine =~ /type/i)) {
                    @{ $tmpArray } = split(/\s+/, $nLine);
                    my $audioInfo;
                    $audioInfo->{PID} = $tmpArray->[2];
                    $audioInfo->{TYPE} = $tmpArray->[4];
                    if (($fileContent->[$i+1] =~ /^audio/i) and ($fileContent->[$i+1] =~ /lang/i)) {
                        $nLine = $fileContent->[$i+1];
                        $nLine =~ s/audio://gi;
                        $nLine =~ s/language://gi;
                        $nLine =~ s/\s//gi;
                        $audioInfo->{LANG} = $nLine;
                    }
                    
                    if (($fileContent->[$i+2] =~ /^audio/i) and ($fileContent->[$i+2] =~ /stream id/i)) {
                        $nLine = $fileContent->[$i+2];
                        $nLine =~ s/\D//g;
                        $audioInfo->{StreamIndex} = $nLine;
                    }
                    push(@{ $jsonObject->{PNRS}->{$nPNR}->{AUDIO} }, $audioInfo);
                }
            }
            elsif($nLine =~ /crc32/i){
                last PMTPARSER;
            }
            $i++;
            next PMTPARSER;
        }
        undef $nextline;
        next ARRAYPARSER;
    }
    elsif($nextline =~ /^sdt/i){
        my $nPNR = 0;
        if ($nextline =~ /tsid/i) {
            undef $nextline;
            next ARRAYPARSER;
        }
        elsif($nextline =~ /sid/i){
            $nLine = $nextline;
            $nLine =~ s/\D//g;
            $nPNR = $nLine;
            if ($fileContent->[$i+2] =~ /service/i) {
                @{ $tmpArray } = split(/\s+/, $fileContent->[$i+2]);
                my $x = shift @{ $tmpArray }; $x = shift @{ $tmpArray };
                $jsonObject->{PNRS}->{$nPNR}->{ServiceName} = join(' ', @{ $tmpArray });
                undef $tmpArray;
                undef $nLine;
            }
        }
    }
    else{}
}

$config->{o_filename} = $config->{wdir} . '/' . $config->{instance_name}."-out.txt";

my $string;
open(OUTTXT, '>', $config->{o_filename}) or do { warn "Cant open file $!" };

if (!defined($jsonObject)) {
    print OUTTXT "Status: 0\n";
    close OUTTXT;
    exit 0;
}
else{
    print OUTTXT "Status: 1\n";
}

foreach my $ll (@{ $jsonObject->{PNR_ARRAY} }){
    if (defined($jsonObject->{PNRS}->{$ll}->{VIDEO}->{TYPE})) {
        if ($config->{transponder_type}) {
            $string = $ll . ' = ' . $config->{HDVmp4Amp2};
        }   
        else{
            if ( $jsonObject->{PNRS}->{$ll}->{VIDEO}->{TYPE} =~ /mpeg-4/i ) {
                $string = $ll . ' = ' . $config->{SDVmp4Amp2};
            }
            elsif( $jsonObject->{PNRS}->{$ll}->{VIDEO}->{TYPE} =~ /mpeg-2/i ){
                $string = $ll . ' = ' . $config->{SDVmp2Amp2};
            }
        }
        $string .= '#pnr=1';
        $string .= '&set_pnr='. $ll;
        $string .= '&map.256=' . $jsonObject->{PNRS}->{$ll}->{VIDEO}->{PID};
        
        my $audioPidsArray;
        @{ $audioPidsArray } = (257, 258, 259, 260, 261, 262, 263, 264, 265, 266);
        foreach my $lm (@{ $jsonObject->{PNRS}->{$ll}->{AUDIO} }){
            $string .= '&map.' . shift(@{ $audioPidsArray }) . '=' . $lm->{PID};
        }
        
        if (scalar( @{ $audioPidsArray } ) > 0) {
            $string .= '&filter=' . join(',', @{ $audioPidsArray }); 
        }
        
        print OUTTXT $string, "\n";
    }
}
close OUTTXT;

exit 0;