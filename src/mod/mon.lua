local content = ""
local _t = 5

sysConfig.rootPath = "/etc/astra/"
sysConfig.perlPath = "/usr/bin/perl "
sysConfig.logfilesDir = sysConfig.rootPath .. "logs/"
sysConfig.pidfilesDir = sysConfig.rootPath .. "pids/"
sysConfig.tmpDir = sysConfig.rootPath .. "tmp/"
sysConfig.logfile = sysConfig.logfilesDir .. sysConfig.instanceName .. ".log"
sysConfig.pidfile = sysConfig.pidfilesDir .. sysConfig.instanceName .. ".pid"
sysConfig.astraExecutable = "/usr/bin/astra "
sysConfig.analyzerPath = sysConfig.rootPath .. "mod/analyze.lua"
sysConfig.pidCheckerPath = sysConfig.rootPath .. "mod/checkPid.pl -p "
sysConfig.analyzerStarterScript = sysConfig.rootPath .. "mod/StartAnalyzer.pl -c"
sysConfig.tParserScriptPath = sysConfig.rootPath .. "mod/ap.pl"
sysConfig.hostnameExecutablePath = "/bin/hostname"

sysConfig.globalDebug = false

if(sysConfig.hostname == 0)then
    local cmdHandler = io.popen(sysConfig.hostnameExecutablePath)
    sysConfig.hostname = tostring(cmdHandler:read("*a"))
    sysConfig.hostname = string.gsub(sysConfig.hostname, "\n$", "")
end

analyzerObjects = {}

log.set({   stdout = sysConfig.stdoutEnable,
    debug = sysConfig.debugEnable,
    syslog = sysConfig.instanceName,
    filename = sysConfig.logfile })

pidfile(sysConfig.pidfile)


function getPidOfProcess(pidfilename)
    local analyzerPid
    if utils.stat(pidfilename).type ~= "file" then
        return -1
    end

    local pidFile = assert(io.open(pidfilename, "r"))
    analyzerPid = tonumber(pidFile:read("*all"))
    pidFile:close()

    return analyzerPid
end


myPid = getPidOfProcess(sysConfig.pidfile)
if(myPid <= 0)then
    log.error("Cant get parent process pid. Exiting")
    astra.exit(1)
end


-- settings tuner
local tune_conf = {
    name            = adapterConfig.name,
    type            = adapterConfig.type,
    adapter         = adapterConfig.adapter,
    lnb_sharing     = adapterConfig.lnb_sharing,
    frequency       = adapterConfig.frequency,
    polarization    = adapterConfig.polarization,
    symbolrate      = adapterConfig.symbolrate,
    lof1            = adapterConfig.lof1,
    lof2            = adapterConfig.lof2,
    slof            = adapterConfig.slof,
    modulation      = adapterConfig.modulation,
    device          = adapterConfig.device,
    budget          = false,                                                                                    --adapterConfig.budget,
    fec             = adapterConfig.fec,
    diseqc          = adapterConfig.diseqc,
    buffer_size     = 40,
    enable          = adapterConfig.enable,
    callback = function(data)
        content =   "type=dvb"..
                "&stream="      .. adapterConfig.name ..
                "&dvbType="     .. adapterConfig.type ..
                "&adapter="     .. adapterConfig.adapter ..
                "&frequency="   .. adapterConfig.frequency ..
                "&polarization=".. adapterConfig.polarization ..
                "&symbolrate="  .. adapterConfig.symbolrate ..
                "&diseqc="      .. adapterConfig.diseqc ..
                "&server="      .. sysConfig.hostname ..
                "&ppid="        .. myPid ..
                "&instance="    .. adapterConfig.id ..
                "&signal="      .. data.signal ..
                "&snr="         .. data.snr ..
                "&status="      .. data.status ..
                "&ber="         .. data.ber ..
                "&unc="         .. data.unc
        send_monitor(content)
    end
}

function send_monitor(content)
    http_request({
        host = "127.0.0.1",
        path = "/putdata",
        method = "POST",
        content = content,
        port = 8080,
        headers = {
            "User-Agent: Astra v."  .. astra.version,
            "Host: "                .. sysConfig.hostname,
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: "      .. #content,
            "Connection: close",
        },
        callback = function(s,r)
        end
    })
end

if (sysConfig.localAddress == 0)then
    local tRouteTableDescriptor = io.popen('/bin/netstat -rn')
    local tRouteTableContent = tRouteTableDescriptor:read('*all')
    local tRouteString = string.match(tRouteTableContent, '224.0.0.0.*')
    local tWords = {}
    for tWord in string.gmatch(tRouteString, '%g+')do
        table.insert(tWords, tWord)
    end
    local tInterfaceName = tWords[8]
    sysConfig.localAddress = ifaddr_list[tInterfaceName].ipv4[1]

end

function strSplit(s)
  local list = {}
  for vVal in string.gmatch(s, "[^%s]+") do
    string.gsub(vVal, "%s+", "")
    table.insert(list, vVal)
  end
  return list
end

function fileCheck(fileNameForCheck)
  local fileHandler = io.open(fileNameForCheck, "r")
  if fileHandler == nil then
    return false
  else
    return true
  end
end

function getTableSize(sTable)
  count = 0
  for k, v in pairs(sTable) do
    count = count + 1
  end
  return count
end

local checkResponse = os.execute("ls ", sysConfig.tmpDir)
if checkResponse == false then
  local mkdirResult = os.execute("mkdir ", sysConfig.tmpDir)
  if mkdirResult == false then
    log.error("Cant make temp file directory at " .. sysConfig.tmpDir .. " Terminating")
    astra.abort()
  end
end

pidListFile = sysConfig.tmpDir .. sysConfig.instanceName .. "-out.txt"

-- preparing perl script
-- -n 36_11996_H -a 0 -f 11996 -p H -s 27500 -l 10750:10750:10750 -t S2 -d 2 -b 

local tParser = sysConfig.perlPath .. sysConfig.tParserScriptPath
tParser = tParser .. " -w " .. sysConfig.tmpDir
tParser = tParser .. " -n " .. sysConfig.instanceName
tParser = tParser .. " -a " .. adapterConfig.adapter
tParser = tParser .. " -f " .. adapterConfig.frequency
tParser = tParser .. " -p " .. adapterConfig.polarization
tParser = tParser .. " -s " .. adapterConfig.symbolrate
tParser = tParser .. " -l " .. adapterConfig.lof1 .. ":" .. adapterConfig.lof2 .. ":" .. adapterConfig.slof
tParser = tParser .. " -t " .. adapterConfig.type
tParser = tParser .. " -d " .. adapterConfig.diseqc

if sysConfig.TType ~= nil then
  tParser = tParser .. " -b "
end

tParser = tParser .. " > /dev/null "

local tPReturnCode = os.execute(tParser)
if tPReturnCode == false then
  log.error("Cant execute perl script. Terminating.");
  astra.abort()
end

local fc = fileCheck(pidListFile)
if fc == false then
  log.error("Reserve list file does not exists. Terminatin.")
  astra.abort()
end

reservedStreamsTable = {}
fileContent = {}
statusArray = {}

for line in io.lines(pidListFile) do
  table.insert(fileContent, line)
end

os.remove(pidListFile)

for value in string.gmatch(fileContent[1], "[^%s]+") do
  table.insert(statusArray, value)
end

if tonumber(statusArray[2]) == 1 then
  eCount = getTableSize(fileContent)
  for i=2, eCount do
    local rArray = {}
    table.insert(rArray, strSplit(fileContent[i]))
    reservedStreamsTable[rArray[1][1]] = rArray[1][3]
  end
elseif tonumber(statusArray[2]) == 0 then 
  log.error("Seems transponder is free or there was tunning issue. Try to manually scaning for this transponder. Terminating.")
  astra.abort()
else
  log.error("System error. Terminating.")
  astra.abort()
end

channels = {}

for i, thisChannel in ipairs(tChannel) do
    if(thisChannel.mEnable) then
        local nextChannel = {}
        nextChannel.name = thisChannel.mChannelName
        nextChannel.id = thisChannel.mChannelID
        nextChannel.output = {}
        
        -- set input for channel
        local inputParametersString = "dvb://tunerDVB#"
        inputParametersString = inputParametersString .. "pnr=" .. thisChannel.mChannelPNR
        if(thisChannel.mNeedSoftCam) then
            inputParametersString = inputParametersString .. "&cam=" .. thisChannel.mSoftCAM
        elseif (thisChannel.mNeedBISS) then
            inputParametersString = inputParametersString .. "&biss=" .. thisChannel.mBISSKey
        end

        inputParametersString = inputParametersString .. "&cc_limit=1"
        inputParametersString = inputParametersString .. "&pes_limit=1"

        nextChannel.input = {}
        table.insert(nextChannel.input, inputParametersString)
        if(thisChannel.rNeedSoftCAM) then
            local rInputPS = "dvb://tunerDVB#"
            rInputPS = rInputPS .. "pnr=" .. thisChannel.mChannelPNR
            rInputPS = rInputPS .. "&cam=" .. thisChannel.rSoftCAM
            rInputPS = rInputPS .. "&cc_limit=1"
            rInputPS = rInputPS .. "&pes_limit=1"
            table.insert(nextChannel.input, rInputPS)
        end
-----------------------------------------------------------------------------------------------------------        
        if(thisChannel.rInputEnable) then
            if(thisChannel.rInputNeedSoftCAM) then
                thisChannel.rInputURL = thisChannel.rInputURL .. "&cam=" .. thisChannel.rInputSoftCAM
            elseif (thisChannel.rInputNeedBISS) then
                thisChannel.rInputURL = thisChannel.rInputURL .. "&biss=" .. thisChannel.rInputBISSKey
            end

            table.insert(nextChannel.input, thisChannel.rInputURL)
        end
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
        table.insert(nextChannel.input, reservedStreamsTable[tostring(thisChannel.mChannelPNR)])
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
        
        if(thisChannel.mDisableSwitch) then
            table.insert(nextChannel.input, "stop://")
        end

-----------------------------------------------------------------------------------------------------------
        if(sysConfig.globalDebug == true) then
          for kK, vV in pairs(nextChannel.input) do
            kS = "Input: " .. kK .. " : " .. vV
            log.info(kS)
          end
        end
-----------------------------------------------------------------------------------------------------------

        --set output for channel
        for p, nextOutput in pairs(thisChannel.mOutpts) do
            local outputSettings = {}
            nextChannel.sOutput = {}
            if(nextOutput.Proto == "UDP") then
                local outputParametersString = "udp://"
                outputParametersString = outputParametersString .. sysConfig.localAddress .. "@"
                outputParametersString = outputParametersString .. nextOutput.IP .. ":"
                outputParametersString = outputParametersString .. nextOutput.Port
                if(nextOutput.NeedRemux) then
                  --outputParametersString = outputParametersString .. "#remux"
                  --outputParametersString = outputParametersString .. "&rate=" .. nextOutput.TSRate
                  --outputParametersString = outputParametersString .. "&sync"
                  --outputParametersString = outputParametersString .. "#sync"
                  --outputParametersString = outputParametersString .. "&cbr=" .. (nextOutput.TSRate / 1000000)
                end

                outputSettings.outString = outputParametersString
                outputSettings.Proto = nextOutput.Proto
                outputSettings.toIPaddr = nextOutput.IP
                outputSettings.toPort = nextOutput.Port
                outputSettings.PNR = thisChannel.mChannelPNR
                outputSettings.channelID = thisChannel.mChannelID
                outputSettings.Mountpoint = nextOutput.MountPoint
                outputSettings.TSRate = (nextOutput.TSRate / 1000)
                table.insert(nextChannel.output, outputParametersString)
                table.insert(nextChannel.sOutput, outputSettings)

            elseif(nextOutput.Proto == "HTTP") then
                local outputParametersString = "http://"
                outputParametersString = outputParametersString .. nextOutput.IP .. ":"
                outputParametersString = outputParametersString .. nextOutput.Port .. "/"
                outputParametersString = outputParametersString .. nextOutput.MountPoint

                outputSettings.outString = outputParametersString
                outputSettings.Proto = nextOutput.Proto
                outputSettings.toIPaddr = nextOutput.IP
                outputSettings.toPort = nextOutput.Port
                outputSettings.PNR = thisChannel.mChannelPNR
                outputSettings.channelID = thisChannel.mChannelID
                outputSettings.Mountpoint = nextOutput.MountPoint
                outputSettings.TSRate = (nextOutput.TSRate / 1000)
                table.insert(nextChannel.output, outputParametersString)
                table.insert(nextChannel.sOutput, outputSettings)
            end
            
-----------------------------------------------------------------------------------------------------------
            if(sysConfig.globalDebug == true) then
              for mK, mV in pairs(nextChannel.output) do
                mS = "Output: " .. mK .. " : " .. mV
                log.info(mS)
              end
            end
-----------------------------------------------------------------------------------------------------------
            
            table.insert(channels, nextChannel)
        end
    end
end

function startAnalyzer(analyzerConfig)
    local result = 0
    local commandString = ""
    commandString = commandString .. sysConfig.perlPath
    commandString = commandString .. sysConfig.analyzerStarterScript
    commandString = commandString .. " \""
    commandString = commandString .. sysConfig.astraExecutable
    commandString = commandString .. sysConfig.analyzerPath

    commandString = commandString .. " -t " .. analyzerConfig.timeout
    commandString = commandString .. " -s " .. analyzerConfig.serverName
    commandString = commandString .. " -u " .. analyzerConfig.instanceName
    commandString = commandString .. " -a " .. analyzerConfig.adapterNumber
    commandString = commandString .. " -c " .. analyzerConfig.channelID
    commandString = commandString .. " -b " .. analyzerConfig.channelName
    commandString = commandString .. " -r " .. analyzerConfig.transponderID
    commandString = commandString .. " -x " .. analyzerConfig.tsRate
    commandString = commandString .. " -p " .. analyzerConfig.PNR
    commandString = commandString .. " -z " .. analyzerConfig.parentPid
    commandString = commandString .. " " .. analyzerConfig.inputURL

    commandString = commandString .. "\""

    local cmdHandler = io.popen(commandString)
    return 1
end

function isChildRunning(pidOfChild)
    local isChildRun
    local cmdString = sysConfig.pidCheckerPath .. pidOfChild
    local cmdHandler = io.popen(cmdString)
    isChildRun = tonumber( cmdHandler:read("*a") )
    if(isChildRun == 0)then
        return -1
    else
        return 1
    end
end

tunerDVB = dvb_input(tune_conf)

for q,item in pairs(channels) do
    make_channel(item)

    local thisAnalyzer = {}
    thisAnalyzer.analyzerPidfilename = sysConfig.pidfilesDir ..
            "analyze-" .. sysConfig.instanceName ..
            "-" .. item.name .. ".pid"

    for index,output in pairs(item.sOutput)do
        if(output.Proto == "UDP")then
            thisAnalyzer.timeout = 5
            thisAnalyzer.serverName = sysConfig.hostname
            thisAnalyzer.instanceName = sysConfig.instanceName
            thisAnalyzer.adapterNumber = tune_conf.adapter
            thisAnalyzer.channelID = output.channelID
            thisAnalyzer.channelName = item.name
            thisAnalyzer.transponderID = tune_conf.name
            thisAnalyzer.tsRate = output.TSRate
            thisAnalyzer.PNR = output.PNR
            thisAnalyzer.parentPid = myPid
            thisAnalyzer.inputURL = "udp://" .. output.toIPaddr .. ":" .. output.toPort
        end
    end

    if utils.stat(thisAnalyzer.analyzerPidfilename).type ~= "file" then
        startAnalyzer(thisAnalyzer)
    else
        local childPid = getPidOfProcess(thisAnalyzer.analyzerPidfilename)
        if(isChildRunning(childPid) <= 0)then
            startAnalyzer(thisAnalyzer)
        end
    end
    thisAnalyzer = nil
end