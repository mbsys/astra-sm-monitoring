#!/usr/bin/env astra
--
-- Created by IntelliJ IDEA.
-- User: mehdi
-- Date: 12/30/15
-- Time: 5:28 PM
-- To change this template use File | Settings | File Templates.
--

-- sample cli: /home/mbsys/astra-sm-sm_060815 analyze.lua -t 5 -p 10901 -s IRD-03 -u Turksat -a 3 -c 10901 -b NTV_AVRUPA -r 420100 -x 12000 udp://234.162.13.2:1234

arg_n = nil
input_url = nil
senderConfiguration = {}
senderConfiguration.logColored = true
senderConfiguration.logToSTDOUT = true
senderConfiguration.logDebug = true
senderConfiguration.ParentPid = 0
senderConfiguration.pidCheckerPath = "/etc/astra-sm/mod/checkPid.pl -p "
senderConfiguration.pidfilesDir = "/etc/astra-sm/pids/"
senderConfiguration.logfilesDir = "/etc/astra-sm/logs/"

function sendMonitor(content)
    http_request({
        host = "127.0.0.1",
        path = "/putdata",
        method = "POST",
        content = content,
        port = 8080,
        headers = {
            "User-Agent: Astra v."  .. astra.version,
            "Host: "                .. senderConfiguration.serverName,
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: "      .. #content,
            "Connection: close",
        },
        callback = function(s,r)
        end
    })
end

pidCheckerStatus = 0
pidCheckerTimer = 0

function on_analyze(instance, data, senderConfig)
    if data.error then
        log.error(data.error)
    elseif data.psi then
        if dump_psi_info[data.psi] then
            dump_psi_info[data.psi]("", data)
        else
            log.error("Unknown PSI: " .. data.psi)
        end
    elseif data.analyze then
        local bitrate = 0
        local inputErrors = {}

        local isScrambled = 0
        if(data.total.scrambled == true)then
            isScrambled = 1
        end

        local isActive = 0
        if(data.on_air == true) then
            isActive = 1
        end

        inputErrors.values = {}
        inputErrors.values.cc_error = 0
        inputErrors.values.pes_error = 0
        inputErrors.values.sc_error = 0
        inputErrors.messages = {}
        inputErrors.messages.cc_errors = {}
        inputErrors.messages.pes_errors = {}
        inputErrors.messages.sc_errors = {}
        for counter,item in pairs(data.analyze) do
            local tempMessage
            if(item.pid ~= 8191)then
                bitrate = bitrate + item.bitrate
            end

            if item.cc_error > 0 then
                inputErrors.values.cc_error = item.cc_error
                tempMessage = "PID:" .. tostring(item.pid) .. "=" .. tostring(item.cc_error) .. " "
                log.info(tempMessage)
                table.insert(inputErrors.messages.cc_errors, tempMessage)
                tempMessage = nil
            end

            if item.pes_error > 0 then
                inputErrors.values.pes_error = item.pes_error
                tempMessage = "PID:" .. tostring(item.pid) .. "=" .. tostring(item.pes_error) .. " "
                log.info(tempMessage)
                table.insert(inputErrors.messages.pes_errors, tempMessage)
                tempMessage = nil
            end

            if item.sc_error > 0 then
                inputErrors.values.sc_error = item.sc_error
                tempMessage = "PID:" .. tostring(item.pid) .. "=" .. tostring(item.sc_error) .. " "
                log.info(tempMessage)
                table.insert(inputErrors.messages.sc_errors, tempMessage)
                tempMessage = nil
            end
        end

        content = "type=channel"..
                "&server="              .. senderConfig.serverName ..
                "&ppid="                .. senderConfig.ParentPid ..
                "&instance="            .. senderConfig.instanceName ..
                "&adapter="             .. senderConfig.adapterNumber ..
                "&channel_id="          .. senderConfig.channelID ..
                "&channelName="         .. senderConfig.channelName ..
                "&transponder="         .. senderConfig.transponderID ..
                "&isScrambled="         .. isScrambled ..
                "&tsRate="              .. senderConfig.tsRate ..
                "&bitrate="             .. bitrate ..
                "&dvbErrors="           .. inputErrors.values.cc_error ..
                "&descramblerErrors="   .. inputErrors.values.pes_error ..
                "&active="              .. isActive
        sendMonitor(content)

        --check parent for running
        if(pidCheckerStatus == 0)then
            pidCheckerStatus = 1
            if(pidCheckerTimer == 30)then
                local isParentRunning
                local cmdString = senderConfiguration.pidCheckerPath .. senderConfiguration.ParentPid
                local cmdHandler = io.popen(cmdString)
                isParentRunning = tonumber( cmdHandler:read("*a") )
                --print (isParentRunning)
                if(isParentRunning == 0)then
                    log.info("Parent process does not running")
                    astra.exit()
                end
                pidCheckerTimer = 0
            end
            pidCheckerStatus = 0
            pidCheckerTimer = pidCheckerTimer + 1
        end
    end
end

function start_analyze(instance, addr, senderConfig)
    local conf = parse_url(addr)
    if not conf then
        log.error("[analyze] wrong url format")
        astra.exit(1)
    end

    conf.name = "analyze"
    if conf.pnr == 0 then conf.pnr = senderConfig.PNR end

    if conf.format == "file" then
        log.error("[analyze] this implementation does not support file input")
        astra.exit(1)
    elseif conf.format == "http" then
        log.error("[analyze] this implementation does not support http input")
        astra.exit(1)
    elseif conf.format == "dvb" then
        log.error("[analyze] this implementation does not support http input")
        astra.exit(1)
    end

    instance.input = init_input(conf)
    instance.analyze = analyze({
        upstream = instance.input.tail:stream(),
        name = conf.name,
        callback = function(data)
            on_analyze(instance, data, senderConfig)
        end,
    })
end


options = {
    ["-n"] = function(idx)
        arg_n = tonumber(argv[idx + 1])
        return 1
    end,
    ["-t"] = function(idx)
        senderConfiguration.timeout = tonumber(argv[idx + 1])
        return 1
    end,
    ["-s"] = function(idx)
        senderConfiguration.serverName = argv[idx + 1]
        return 1
    end,
    ["-u"] = function(idx)
        senderConfiguration.instanceName = argv[idx + 1]
        return 1
    end,
    ["-a"] = function(idx)
        senderConfiguration.adapterNumber = tonumber(argv[idx + 1])
        return 1
    end,
    ["-c"] = function(idx)
        senderConfiguration.channelID = argv[idx + 1]
        return 1
    end,
    ["-b"] = function(idx)
        senderConfiguration.channelName = argv[idx + 1]
        return 1
    end,
    ["-r"] = function(idx)
        senderConfiguration.transponderID = argv[idx + 1]
        return 1
    end,
    ["-x"] = function(idx)
        senderConfiguration.tsRate = tonumber(argv[idx + 1])
        return 1
    end,
    ["-p"] = function(idx)
        senderConfiguration.PNR = tonumber(argv[idx + 1])
        return 1
    end,
    ["-z"] = function(idx)
        senderConfiguration.ParentPid = tonumber(argv[idx + 1])
        return 1
    end,
    ["*"] = function(idx)
        input_url = argv[idx]
        return 1
    end
}

function main()

    if input_url then

        local pidfilePath = senderConfiguration.pidfilesDir ..
                "analyze-" .. senderConfiguration.instanceName ..
                "-" .. senderConfiguration.channelName ..".pid"
        os.remove(pidfilePath)

        pidfile(pidfilePath)

        local logfilePath = senderConfiguration.logfilesDir ..
                "analyze-" .. senderConfiguration.instanceName ..
                "-" .. senderConfiguration.channelName .. ".log"

        log.set({       color = senderConfiguration.logColored,
            stdout = senderConfiguration.lgToSTDOUT,
            debug = senderConfiguration.logDebug,
            filename = logfilePath})

        _G.instance = {}
        start_analyze(_G.instance, input_url, senderConfiguration)
        return
    end

    astra_usage()
end