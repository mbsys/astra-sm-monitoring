#!/usr/bin/astra

log.set({ stdout = false, debug = false, syslog = "smpte100" })
pidfile("smpte100.pid")

make_channel({
    name = "SDVmp2Amp2",
    input = {
        "file:///usr/share/smpte/msd2.ts#loop",
        "stop://",
    },
    output = {
        "udp://232.90.0.1:1234",
   },
})

make_channel({
    name = "SDVmp4Amp2",
    input = {
        "file:///usr/share/smpte/msd4.ts#loop",
        "stop://",
    },
    output = {
        "udp://232.90.0.2:1234",
   },
})

make_channel({
    name = "HDVmp4Amp2",
    input = {
        "file:///usr/share/smpte/mhd.ts#loop",
        "stop://",
    },
    output = {
        "udp://232.90.0.3:1234",
   },
})