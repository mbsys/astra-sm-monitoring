First install some perl modules
    - Just do:
            sudo apt-get install libgetopt-declare-perl libfindbin-libs-perl libproc-background-perl libfile-which-perl

Then, need make alternative streams in SMPTE100 standart. Use scripts in smpte100 folders and media files in media folder.

For HD transponders, just add
    sysConfig.TType = "hd"
to lua config file